package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class Conectar {
    
    private static Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "root";
    private static final String password = "root";
    private static final String url = "jdbc:mysql://localhost:3306/proyecto";
    
    //Con este método nos conectamos a la base de datos
    public Conectar(){
        
        conn = null;
        
        try{
            
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
            
            if(conn != null){    
            }
            
        }catch(Exception e){
            
             JOptionPane.showMessageDialog(null, "Error al conectar a la base de datos" + e);
             
        }
        
    }
    
    //Este método nos retorna la conexión
    public Connection getConnection(){
        return conn;
    }
    
    //Con este método nos desconectamos
    public void desconectar(){
        
        conn = null;
        
    }
    
}