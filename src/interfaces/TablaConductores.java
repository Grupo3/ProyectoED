package interfaces;

import clases.Conectar;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class TablaConductores extends javax.swing.JFrame {

    private Conectar con;
    private Statement stmt;
    private String titulos [] = {"CODIGO", "APELLIDO PATERNO", "APELLIDO MATERNO", "NOMBRES", "SEXO", 
                                 "NACIONALIDAD", "DNI" , "T. LICENCIA", 
                                 "DIRECCION" , "TEL. MOVIL", "TEL. FIJO", "CORREO", "CONTRASEÑA"};
    
    private String fila [] = new String [13];
    private DefaultTableModel modelo;
    
    public TablaConductores() {
        initComponents();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setTitle("Tabla de Conductores");
        
        con = new Conectar();
        
        try {
            stmt = con.getConnection().createStatement();
            
            ResultSet rs = stmt.executeQuery("SELECT * FROM conductores");
            
            modelo = new DefaultTableModel(null, titulos);
            
            while(rs.next()){
                fila[0] = rs.getString(1);
                fila[1] = rs.getString(2);
                fila[2] = rs.getString(3);
                fila[3] = rs.getString(4);
                fila[4] = rs.getString(5);
                fila[5] = rs.getString(6);
                fila[6] = rs.getString(7);
                fila[7] = rs.getString(8);
                fila[8] = rs.getString(9);
                fila[9] = rs.getString(10);
                fila[10] = rs.getString(11);
                fila[11] = rs.getString(12);
                fila[12] = rs.getString(13);
                
                modelo.addRow(fila);
            }
            
            tbl_choferes.setModel(modelo);
            
            TableColumn cc = tbl_choferes.getColumn("CODIGO");
            cc.setMaxWidth(300);
            TableColumn ap = tbl_choferes.getColumn("APELLIDO PATERNO");
            ap.setMaxWidth(300);
            TableColumn am = tbl_choferes.getColumn("APELLIDO MATERNO");
            am.setMaxWidth(300);
            TableColumn nm = tbl_choferes.getColumn("NOMBRES");
            nm.setMaxWidth(300);
            TableColumn sx = tbl_choferes.getColumn("SEXO");
            sx.setMaxWidth(300);
            TableColumn nc = tbl_choferes.getColumn("NACIONALIDAD");
            nc.setMaxWidth(300);
            TableColumn dn = tbl_choferes.getColumn("DNI");
            dn.setMaxWidth(300);
            TableColumn li = tbl_choferes.getColumn("T. LICENCIA");
            li.setMaxWidth(300);
            TableColumn di = tbl_choferes.getColumn("DIRECCION");
            di.setMaxWidth(300);
            TableColumn tm = tbl_choferes.getColumn("TEL. MOVIL");
            tm.setMaxWidth(300);
            TableColumn tf = tbl_choferes.getColumn("TEL. FIJO");
            tf.setMaxWidth(300);
            TableColumn co = tbl_choferes.getColumn("CORREO");
            co.setMaxWidth(300);
            TableColumn ps = tbl_choferes.getColumn("CONTRASEÑA");
            ps.setMaxWidth(300);
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error al recuperar los datos", "AVISO!", JOptionPane.INFORMATION_MESSAGE);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_choferes = new javax.swing.JTable();
        bt_atras = new javax.swing.JButton();
        bt_cerrarSesion = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbl_choferes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_choferes);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1250, 90));

        bt_atras.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bt_atras.setText("ATRAS");
        bt_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_atrasActionPerformed(evt);
            }
        });
        getContentPane().add(bt_atras, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 180, -1, 30));

        bt_cerrarSesion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bt_cerrarSesion.setText("CERRAR SESION");
        bt_cerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_cerrarSesionActionPerformed(evt);
            }
        });
        getContentPane().add(bt_cerrarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 210, -1, -1));

        jLabel1.setToolTipText("");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1250, 240));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_atrasActionPerformed
        
        this.dispose();
        
        VentanaSecretaria administrador = new VentanaSecretaria();
        administrador.setVisible(true);
        
    }//GEN-LAST:event_bt_atrasActionPerformed

    private void bt_cerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_cerrarSesionActionPerformed
        
        VentanaPrincipal v_principal = new VentanaPrincipal();
        v_principal.setLocationRelativeTo(null);
        v_principal.setResizable(false);
        v_principal.setVisible(true);
        
        this.dispose();
        
    }//GEN-LAST:event_bt_cerrarSesionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaConductores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaConductores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaConductores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaConductores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TablaConductores().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt_atras;
    private javax.swing.JButton bt_cerrarSesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_choferes;
    // End of variables declaration//GEN-END:variables
}
